# Git

## Install git on your operating system

## Configure Git
    git config --global user.name "your name/username"
    git config --global user.email "your email"
    git config --global http.sslVerify "false"

## Initialize local repo
    git init

## Check repo status
    git status

## Stage add changes
    git add *

## Stage changes to a file
    git add $fileName

## Commit changes
     git commit -m "commit message"

## Add to remote
    git remote add origin $remoteRepoUrl

## Push to remote repo
    git push -u origin master


---

## Working with an existing repo

## Clone repo
    git clone $repoUrl

## Create and checkout to a new branch
    git checkout -b $branchName

## Create and checkout to an existing branch
    git checkout $branchName

